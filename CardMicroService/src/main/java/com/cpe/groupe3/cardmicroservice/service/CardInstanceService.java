package com.cpe.groupe3.cardmicroservice.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.Random;

import com.cpe.groupe3.commonmicroservice.dto.NotificationAction;
import com.cpe.groupe3.commonmicroservice.exception.NotificationException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.cpe.groupe3.cardmicroservice.exception.CardInstanceNotFoundException;
import com.cpe.groupe3.cardmicroservice.mapper.CardInstanceMapper;
import com.cpe.groupe3.cardmicroservice.mapper.CardMapper;
import com.cpe.groupe3.cardmicroservice.model.CardInstanceModel;
import com.cpe.groupe3.cardmicroservice.model.CardModel;
import com.cpe.groupe3.cardmicroservice.repository.CardInstanceRepository;
import com.cpe.groupe3.cardmicroservicepublic.dto.CardDTO;
import com.cpe.groupe3.cardmicroservicepublic.dto.CardInstanceDTO;
import com.cpe.groupe3.cardmicroservicepublic.dto.CardInstanceTransactionDTO;
import com.cpe.groupe3.commonmicroservice.service.QueueService;
import com.cpe.groupe3.transactionmicroservicepublic.consumer.TransactionRestConsumer;
import com.cpe.groupe3.transactionmicroservicepublic.dto.UpdateTransactionDTO;
import com.cpe.groupe3.usermicroservicepublic.consumer.UserRestConsumer;

@Service
public class CardInstanceService extends QueueService implements ICardInstanceService{
	@Autowired
	private UserRestConsumer userRestConsumer;
	@Autowired
	private TransactionRestConsumer transactionRestConsumer;
	@Autowired
	private CardInstanceRepository cardInstanceRepository;
	@Autowired
	private CardService cardService;
	
	private Random rand;

	public CardInstanceService() {
		this.rand = new Random();
	}
	
	public List<CardInstanceDTO> getAllCardInstances() {
		List<CardInstanceDTO> cLightList=new ArrayList<>();
		for(CardInstanceModel c:cardInstanceRepository.findAll()){
			cLightList.add(CardInstanceMapper.fromCIMtoCIDTO(c));
		}
		return cLightList;
	}
	
	public CardInstanceDTO addCardInstance(CardInstanceDTO card) {
		CardDTO cD = cardService.getCard(Integer.valueOf(card.getIdCard()));
		CardInstanceModel cDb = cardInstanceRepository.save(CardInstanceMapper.fromCIDTOtoCIM(card, CardMapper.fromCardDtoToCardModel(cD)));
		return CardInstanceMapper.fromCIMtoCIDTO(cDb);
	}
	

	public void addCardInstancesToNewUser(Integer userId) throws NotificationException {
		userRestConsumer.get(userId.toString());
		List<CardInstanceModel> cardInstanceList = new ArrayList<>();
		for(int i=0;i<5;i++) {
			CardModel currentCard = cardService.getRandCard();
			CardInstanceModel currentCardInstance = new CardInstanceModel();
			currentCardInstance.setCard(currentCard);
			currentCardInstance.setAttack(rand.nextFloat()*100);
			currentCardInstance.setDefence(rand.nextFloat()*100);
			currentCardInstance.setEnergy(100);
			currentCardInstance.setHp(rand.nextFloat()*100);
			currentCardInstance.setPrice(100);
			currentCardInstance.setUserId(userId);
			cardInstanceList.add(currentCardInstance);
		}
		try{
			cardInstanceRepository.saveAll(cardInstanceList);
		}catch (Exception e){
			throw new NotificationException("There was an error while creating your cards !", userId, NotificationAction.NOTIFY);
		}
		throw new NotificationException("Your cards have been created !", userId, NotificationAction.NOTIFY);
	}
	
	public CardInstanceDTO updateCardInstance(CardInstanceDTO cardInstance) {
		if (cardInstance.getUserId() != null){ // Check that the cardInstance isn't linked to a user that does not exist
			userRestConsumer.get(cardInstance.getUserId().toString());
		}
		getCardInstance(cardInstance.getIdCardInstance()); // Check if CardInstance exists
		CardDTO cD = cardService.getCard(cardInstance.getIdCard());
		CardInstanceModel cDb=cardInstanceRepository.save(CardInstanceMapper.fromCIDTOtoCIM(cardInstance, CardMapper.fromCardDtoToCardModel(cD)));
		return CardInstanceMapper.fromCIMtoCIDTO(cDb);
	}

	/*
	 * It does not matter if the card instance doesn't exist, it'll set the cardOk field to false and rollback the transaction
	 */
	public void updateCardInstanceForTransaction(CardInstanceTransactionDTO cITD) {
		UpdateTransactionDTO uTD = new UpdateTransactionDTO(cITD.getOrderId());
		try{
			updateCardInstance(cITD.getCardInstance());
			uTD.setCardOk(true);
		}catch(Exception e){
			uTD.setCardOk(false);
		}finally{
			transactionRestConsumer.updateTransaction(uTD, cITD.getOrderId());
		}
	}
	
	public CardInstanceDTO getCardInstance(Integer id) {
		Optional<CardInstanceModel> rcard;
		rcard= cardInstanceRepository.findById(id);
		if(rcard.isPresent()) {
			return CardInstanceMapper.fromCIMtoCIDTO(rcard.get());
		}
		throw new CardInstanceNotFoundException("Card Instance not found");
	}
	
	public void deleteCardInstance(Integer id) {
		cardInstanceRepository.deleteById(id);
	}

	public List<CardInstanceModel> getAllCardInstancesForUserId(Integer userId) {
		userRestConsumer.get(userId.toString());
		return this.cardInstanceRepository.findByUserId(userId);
	}
	
	public List<CardInstanceModel> getCardInstancesToSell(){
		return this.cardInstanceRepository.findByUserId(null);
	}
}
