package com.cpe.groupe3.cardmicroservice.repository;

import java.util.List;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.cpe.groupe3.cardmicroservice.model.CardInstanceModel;

@Repository
public interface CardInstanceRepository extends CrudRepository<CardInstanceModel, Integer> {
	public List<CardInstanceModel> findByUserId(Integer userId);
}
