package com.cpe.groupe3.cardmicroservice.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.cpe.groupe3.cardmicroservice.model.CardInstanceModel;
import com.cpe.groupe3.cardmicroservicepublic.dto.CardInstanceDTO;
import com.cpe.groupe3.cardmicroservicepublic.dto.CardInstanceTransactionDTO;
import com.cpe.groupe3.commonmicroservice.message.QueueMessage;

@Component
public class DispatcherCardInstanceService implements ICardInstanceService {
	@Autowired
	private CardInstanceService cardInstanceService;

	public List<CardInstanceDTO> getAllCardInstances() {
		return cardInstanceService.getAllCardInstances();
	}

	public CardInstanceDTO addCardInstance(CardInstanceDTO card) {
		return cardInstanceService.addCardInstance(card);
	}

	public void addCardInstancesToNewUser(Integer userId) {
		cardInstanceService.post(new QueueMessage<Integer>(userId, "addCardInstancesToNewUser"));
	}

	public CardInstanceDTO updateCardInstance(CardInstanceDTO cardInstance){
		return cardInstanceService.updateCardInstance(cardInstance);
	}
	
	public void updateCardInstanceForTransaction(CardInstanceTransactionDTO cITD) {
		cardInstanceService.post(new QueueMessage<CardInstanceTransactionDTO>(cITD, "updateCardInstanceForTransaction"));
	}

	public CardInstanceDTO getCardInstance(Integer id) {
		return cardInstanceService.getCardInstance(id);
	}

	public void deleteCardInstance(Integer id) {
		cardInstanceService.deleteCardInstance(id);
		
	}

	public List<CardInstanceModel> getAllCardInstancesForUserId(Integer userId) {
		return cardInstanceService.getAllCardInstancesForUserId(userId);
	}
	
	public List<CardInstanceModel> getCardInstancesToSell(){
		return cardInstanceService.getCardInstancesToSell();
	}
}
