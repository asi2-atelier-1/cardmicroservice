package com.cpe.groupe3.cardmicroservice.mapper;

import com.cpe.groupe3.cardmicroservice.model.CardInstanceModel;
import com.cpe.groupe3.cardmicroservice.model.CardModel;
import com.cpe.groupe3.cardmicroservicepublic.dto.CardInstanceDTO;

public class CardInstanceMapper {
	public static CardInstanceDTO fromCIMtoCIDTO(CardInstanceModel cIM) {
		CardInstanceDTO cDto = new CardInstanceDTO();
		cDto.setIdCardInstance(cIM.getIdCardInstance());
		cDto.setIdCard(cIM.getCard().getIdCard());
		cDto.setEnergy(cIM.getEnergy());
		cDto.setHp(cIM.getHp());
		cDto.setDefence(cIM.getDefence());
		cDto.setAttack(cIM.getAttack());
		cDto.setPrice(cIM.getPrice());
		cDto.setUserId(cIM.getUserId());
		return cDto;
	}
	
	public static CardInstanceModel fromCIDTOtoCIM(CardInstanceDTO cID, CardModel cM) {
		CardInstanceModel cIM = new CardInstanceModel();
		cIM.setIdCardInstance(cID.getIdCardInstance());
		cIM.setCard(cM);
		cIM.setEnergy(cID.getEnergy());
		cIM.setHp(cID.getHp());
		cIM.setDefence(cID.getDefence());
		cIM.setAttack(cID.getAttack());
		cIM.setPrice(cID.getPrice());
		cIM.setUserId(cID.getUserId());
		return cIM;
	}
}
