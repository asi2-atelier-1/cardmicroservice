package com.cpe.groupe3.cardmicroservice.exception;

public class CardInstanceNotFoundException extends RuntimeException{
    public CardInstanceNotFoundException(String message){
        super(message);
    }
}
