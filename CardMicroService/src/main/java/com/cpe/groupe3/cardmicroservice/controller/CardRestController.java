package com.cpe.groupe3.cardmicroservice.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import com.cpe.groupe3.cardmicroservice.service.CardService;
import com.cpe.groupe3.cardmicroservicepublic.dto.CardDTO;
import com.cpe.groupe3.cardmicroservicepublic.rest.ICardRest;

//ONLY FOR TEST NEED ALSO TO ALLOW CROOS ORIGIN ON WEB BROWSER SIDE
@CrossOrigin
@RestController
public class CardRestController implements ICardRest {
	@Autowired
	private CardService cardService;
	
	public CardRestController() {
	}
	
	@Override
	public List<CardDTO> getAllCards() {
		return cardService.getAllCards();
	}

	@Override
	public CardDTO getCard(@PathVariable Integer id) {
		return cardService.getCard(id);
	}
}
