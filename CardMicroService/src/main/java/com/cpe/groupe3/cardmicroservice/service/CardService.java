package com.cpe.groupe3.cardmicroservice.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.Random;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Service;

import com.cpe.groupe3.cardmicroservice.exception.CardNotFoundException;
import com.cpe.groupe3.cardmicroservice.mapper.CardMapper;
import com.cpe.groupe3.cardmicroservice.model.CardModel;
import com.cpe.groupe3.cardmicroservice.repository.CardRepository;
import com.cpe.groupe3.cardmicroservicepublic.dto.CardDTO;

@Service
public class CardService {
	@Autowired
	private  CardRepository cardRepository;
	
	public CardService() {
	}

	public List<CardDTO> getAllCards() {
		List<CardDTO> cLightList = new ArrayList<>();
		for(CardModel c:cardRepository.findAll()){
			cLightList.add(CardMapper.fromCardModelToCardDTO(c));
		}
		return cLightList;
	}

	public CardDTO getCard(Integer id) {
		Optional<CardModel> rcard;
		rcard = cardRepository.findById(id);
		if(rcard.isPresent()) {
			return CardMapper.fromCardModelToCardDTO(rcard.get());
		}
		throw new CardNotFoundException("Card not found");
	}
	
	public void addCard(CardModel cardModel) {
		cardRepository.save(cardModel);
	}

	public void updateCard(CardModel cardModel) {
		getCard(cardModel.getIdCard());
		cardRepository.save(cardModel);
	}

	public CardModel getRandCard() {
		List<CardModel> cardList = new ArrayList<CardModel>();
		for(CardModel c:cardRepository.findAll()){
			cardList.add(c);
		}
		if( cardList.size()>0) {
			Random rand=new Random();
			int rindex=rand.nextInt(cardList.size()-1);
			return cardList.get(rindex);
		}
		return null;
	}

	/**
	 * Executed after application start
	 */
	@EventListener(ApplicationReadyEvent.class)
	public void doInitAfterStartup() {
		for(int i=0;i<10;i++){
			CardModel card =new CardModel("name"+i,"description"+i,"family"+i,"affinity"+i,"http://medias.3dvf.com/news/sitegrab/gits2045.jpg","https://cdn.animenewsnetwork.com/thumbnails/fit600x1000/cms/feature/89858/05.jpg");
			addCard(card);
			i++;
		}
	}
}
