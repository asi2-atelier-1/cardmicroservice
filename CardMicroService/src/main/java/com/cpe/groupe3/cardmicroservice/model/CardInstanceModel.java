package com.cpe.groupe3.cardmicroservice.model;

import javax.persistence.Entity;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

@Entity
@JsonIdentityInfo(generator= ObjectIdGenerators.PropertyGenerator.class, property="id")
public class CardInstanceModel {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Integer idCardInstance;
	private float energy;
	private float hp;
	private float defence;
	private float attack;
	private float price;
	private Integer userId;

	@ManyToOne
	@JoinColumn(columnDefinition="Integer", name = "id_card", foreignKey = @ForeignKey(name = "fk_card_model_id_card"))
	private CardModel card;


	public CardInstanceModel() {
		super();
	}
	
	public CardInstanceModel( CardInstanceModel cInstanceModel) {
		this.energy=cInstanceModel.getEnergy();
		this.hp=cInstanceModel.getHp();
		this.defence=cInstanceModel.getDefence();
		this.attack=cInstanceModel.getAttack();
		this.price=cInstanceModel.getPrice();
		this.userId = cInstanceModel.getUserId();
		this.card = cInstanceModel.card;
	}

	public CardInstanceModel(float energy, float hp, float defence, float attack,float price, Integer userId, CardModel card) {
		this.energy = energy;
		this.hp = hp;
		this.defence = defence;
		this.attack = attack;
		this.price=this.computePrice();
		this.userId = userId;
		this.card = card;
	}
	
	public CardModel getCard() {
		return card;
	}

	public void setCard(CardModel card) {
		this.card = card;
	}
	
	public float getEnergy() {
		return energy;
	}
	public void setEnergy(float energy) {
		this.energy = energy;
	}
	public float getHp() {
		return hp;
	}
	public void setHp(float hp) {
		this.hp = hp;
	}
	public float getDefence() {
		return defence;
	}
	public void setDefence(float defence) {
		this.defence = defence;
	}
	public float getAttack() {
		return attack;
	}
	public void setAttack(float attack) {
		this.attack = attack;
	}

	public float getPrice() {
		return price;
	}
	public void setPrice(float price) {
		this.price = price;
	}
	
	public Integer getUserId() {
		return userId;
	}
	
	public void setUserId(Integer userId) {
		this.userId = userId;
	}


	public float computePrice() {
		return this.hp * 20 + this.defence*20 + this.energy*20 + this.attack*20;
	}

	public Integer getIdCardInstance() {
		return idCardInstance;
	}

	public void setIdCardInstance(Integer idCardInstance) {
		this.idCardInstance = idCardInstance;
	}
}
