package com.cpe.groupe3.cardmicroservice.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.cpe.groupe3.cardmicroservice.mapper.CardInstanceMapper;
import com.cpe.groupe3.cardmicroservice.model.CardInstanceModel;
import com.cpe.groupe3.cardmicroservice.service.DispatcherCardInstanceService;
import com.cpe.groupe3.cardmicroservicepublic.dto.CardInstanceDTO;
import com.cpe.groupe3.cardmicroservicepublic.dto.CardInstanceTransactionDTO;
import com.cpe.groupe3.cardmicroservicepublic.rest.ICardInstanceRest;

//ONLY FOR TEST NEED ALSO TO ALLOW CROOS ORIGIN ON WEB BROWSER SIDE
@CrossOrigin
@RestController
public class CardInstanceRestController implements ICardInstanceRest {
	@Autowired
	private DispatcherCardInstanceService dispatcherCardInstanceService;

	public CardInstanceRestController() {
	}
	
	@Override
	public List<CardInstanceDTO> getAllCardInstances() {
		return dispatcherCardInstanceService.getAllCardInstances();
	}
	
	@Override
	public CardInstanceDTO getCardInstance(@PathVariable Integer id) {
		return dispatcherCardInstanceService.getCardInstance(id);

	}
	
	@Override
	public CardInstanceDTO addCardInstance(@RequestBody CardInstanceDTO card) {
		return dispatcherCardInstanceService.addCardInstance(card);
	}
	
	@Override
	public void addCardInstancesToNewUser(@PathVariable Integer userId) {
		dispatcherCardInstanceService.addCardInstancesToNewUser(userId);
	}
	
	@Override
	public CardInstanceDTO updateCardInstance(@RequestBody CardInstanceDTO cardInstance,@PathVariable Integer id) {
		cardInstance.setIdCardInstance(Integer.valueOf(id));
		return dispatcherCardInstanceService.updateCardInstance(cardInstance);
	}
	
	@Override
	public void updateCardInstanceForTransaction(@RequestBody CardInstanceTransactionDTO cITD,@PathVariable Integer id) {
		cITD.getCardInstance().setIdCardInstance(Integer.valueOf(id));
		dispatcherCardInstanceService.updateCardInstanceForTransaction(cITD);
	}
	
	@Override
	public void deleteCardInstance(@PathVariable Integer id) {
		dispatcherCardInstanceService.deleteCardInstance(Integer.valueOf(id));
	}

	@Override
	public List<CardInstanceDTO> getCardInstancesForUser(@PathVariable Integer userId) {
		List<CardInstanceDTO> list=new ArrayList<>();
		for(CardInstanceModel c : dispatcherCardInstanceService.getAllCardInstancesForUserId(userId)){
			CardInstanceDTO cLight = CardInstanceMapper.fromCIMtoCIDTO(c);
			list.add(cLight);
		}
		return list;
	}
	
	@Override
	public List<CardInstanceDTO> getCardInstancesToSell() {
		List<CardInstanceDTO> list=new ArrayList<>();
		for(CardInstanceModel c : dispatcherCardInstanceService.getCardInstancesToSell()){
			CardInstanceDTO cLight = CardInstanceMapper.fromCIMtoCIDTO(c);
			list.add(cLight);
		}
		return list;
	}
}
