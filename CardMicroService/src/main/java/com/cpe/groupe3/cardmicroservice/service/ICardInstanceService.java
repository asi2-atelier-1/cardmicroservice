package com.cpe.groupe3.cardmicroservice.service;

import java.util.List;

import com.cpe.groupe3.cardmicroservice.model.CardInstanceModel;
import com.cpe.groupe3.cardmicroservicepublic.dto.CardInstanceDTO;
import com.cpe.groupe3.cardmicroservicepublic.dto.CardInstanceTransactionDTO;
import com.cpe.groupe3.commonmicroservice.exception.NotificationException;

public interface ICardInstanceService {
	public List<CardInstanceDTO> getAllCardInstances();
	public CardInstanceDTO addCardInstance(CardInstanceDTO card);
	public void addCardInstancesToNewUser(Integer userId) throws NotificationException;
	public CardInstanceDTO updateCardInstance(CardInstanceDTO cardInstanceModel);
	public void updateCardInstanceForTransaction(CardInstanceTransactionDTO cIT);
	public CardInstanceDTO getCardInstance(Integer id);
	public void deleteCardInstance(Integer id);
	public List<CardInstanceModel> getAllCardInstancesForUserId(Integer userId);
	public List<CardInstanceModel> getCardInstancesToSell();
}
