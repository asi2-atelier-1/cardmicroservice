package com.cpe.groupe3.cardmicroservice.exception;

import org.springframework.core.Ordered;
import org.springframework.core.annotation.Order;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import com.cpe.groupe3.commonmicroservice.dto.ErrorDTO;

@RestControllerAdvice
@Order(Ordered.LOWEST_PRECEDENCE)
public class CardAndCardInstanceExceptionHandler {
    @ExceptionHandler(CardNotFoundException.class)
    public ResponseEntity<ErrorDTO> handleCardNotFoundException(final CardNotFoundException ex){
        return new ResponseEntity<ErrorDTO>(new ErrorDTO("Invalid Card", ex.getMessage(), HttpStatus.NOT_FOUND.value(), CardNotFoundException.class.getSimpleName()), HttpStatus.NOT_FOUND);
    }

    @ExceptionHandler(CardInstanceNotFoundException.class)
    public ResponseEntity<ErrorDTO> handleCardInstanceNotFoundException(final CardInstanceNotFoundException ex){
        return new ResponseEntity<ErrorDTO>(new ErrorDTO("Invalid Card Instance", ex.getMessage(), HttpStatus.NOT_FOUND.value(), CardInstanceNotFoundException.class.getSimpleName()), HttpStatus.NOT_FOUND);
    }
}
