package com.cpe.groupe3.cardmicroservice.mapper;

import com.cpe.groupe3.cardmicroservice.model.CardModel;
import com.cpe.groupe3.cardmicroservicepublic.dto.CardDTO;

public class CardMapper {
	public static CardDTO fromCardModelToCardDTO(CardModel cIM) {
		CardDTO cDto = new CardDTO();
		cDto.setAffinity(cIM.getAffinity());
		cDto.setDescription(cIM.getDescription());
		cDto.setFamily(cIM.getFamily());
		cDto.setIdCard(cIM.getIdCard());
		cDto.setImgUrl(cIM.getImgUrl());
		cDto.setName(cIM.getName());
		cDto.setSmallImgUrl(cIM.getSmallImgUrl());
		return cDto;
	}
	
	public static CardModel fromCardDtoToCardModel(CardDTO cD) {
		CardModel cM = new CardModel();
		cM.setAffinity(cD.getAffinity());
		cM.setDescription(cD.getDescription());
		cM.setFamily(cD.getFamily());
		cM.setIdCard(cD.getIdCard());
		cM.setImgUrl(cD.getImgUrl());
		cM.setName(cD.getName());
		cM.setSmallImgUrl(cD.getSmallImgUrl());
		return cM;
	}
}
