package com.cpe.groupe3.cardmicroservice.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.cpe.groupe3.cardmicroservice.model.CardModel;

@Repository
public interface CardRepository extends CrudRepository<CardModel, Integer> {

}

