package com.cpe.groupe3.cardmicroservicepublic.rest;

import java.util.List;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.cpe.groupe3.cardmicroservicepublic.dto.CardInstanceDTO;
import com.cpe.groupe3.cardmicroservicepublic.dto.CardInstanceTransactionDTO;

public interface ICardInstanceRest {
    public final String ROOT_PATH = "/cardinstances";

    public final String ALL = ROOT_PATH;
    public final String GET = ROOT_PATH + "/{id}";
    public final String ADD = ROOT_PATH;
    public final String ADD_NEW_USER = ROOT_PATH + "/{userId}";
    public final String UPDATE = ROOT_PATH + "/{id}";
    public final String UPDATE_FOR_TRANSACTION = ROOT_PATH + "/transaction/{id}";
    public final String DELETE = ROOT_PATH + "/{id}";
    public final String CARD_INSTANCES_TO_SELL = ROOT_PATH + "/sell";
    public final String CARD_INSTANCES_FOR_USER = ROOT_PATH + "/user/{userId}";

    @RequestMapping(method = RequestMethod.GET, value = ALL)
    public List<CardInstanceDTO> getAllCardInstances();

    @RequestMapping(method = RequestMethod.GET, value = GET)
    public CardInstanceDTO getCardInstance(Integer id);

    @RequestMapping(method = RequestMethod.POST, value = ADD)
    public CardInstanceDTO addCardInstance(CardInstanceDTO card);
    
    @RequestMapping(method = RequestMethod.POST, value = ADD_NEW_USER)
    public void addCardInstancesToNewUser(Integer userId);
    
    @RequestMapping(method = RequestMethod.PATCH, value = UPDATE)
    public CardInstanceDTO updateCardInstance(CardInstanceDTO card, Integer id);
    
    @RequestMapping(method = RequestMethod.PATCH, value = UPDATE_FOR_TRANSACTION)
    public void updateCardInstanceForTransaction(CardInstanceTransactionDTO cIT, Integer id);
    
    @RequestMapping(method = RequestMethod.DELETE, value = DELETE)
    public void deleteCardInstance(Integer id);
    
    @RequestMapping(method=RequestMethod.GET, value = CARD_INSTANCES_TO_SELL)
	public List<CardInstanceDTO> getCardInstancesToSell();
    
    @RequestMapping(method=RequestMethod.GET, value = CARD_INSTANCES_FOR_USER)
	public List<CardInstanceDTO> getCardInstancesForUser(Integer userId);
}
