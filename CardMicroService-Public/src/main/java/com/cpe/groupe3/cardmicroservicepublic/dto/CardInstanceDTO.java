package com.cpe.groupe3.cardmicroservicepublic.dto;

import java.io.Serializable;

public class CardInstanceDTO implements Serializable {
	private static final long serialVersionUID = 4926792021688822843L;
	private Integer idCardInstance;
	private Integer idCard;
	private float energy;
	private float hp;
	private float defence;
	private float attack;
	private float price;
	private Integer userId;

	public CardInstanceDTO() {
	}

	public Integer getIdCardInstance() {
		return idCardInstance;
	}

	public void setIdCardInstance(Integer idCardInstance) {
		this.idCardInstance = idCardInstance;
	}
	
	public Integer getIdCard() {
		return idCard;
	}

	public void setIdCard(Integer idCard) {
		this.idCard = idCard;
	}

	public float getEnergy() {
		return energy;
	}

	public void setEnergy(float energy) {
		this.energy = energy;
	}

	public float getHp() {
		return hp;
	}

	public void setHp(float hp) {
		this.hp = hp;
	}

	public float getDefence() {
		return defence;
	}

	public void setDefence(float defence) {
		this.defence = defence;
	}

	public float getAttack() {
		return attack;
	}

	public void setAttack(float attack) {
		this.attack = attack;
	}

	public float getPrice() {
		return price;
	}

	public void setPrice(float price) {
		this.price = price;
	}

	public Integer getUserId() {
		return userId;
	}

	public void setUserId(Integer userId) {
		this.userId = userId;
	}

}
