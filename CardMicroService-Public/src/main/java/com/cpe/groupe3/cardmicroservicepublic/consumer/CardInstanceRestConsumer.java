package com.cpe.groupe3.cardmicroservicepublic.consumer;

import java.util.Arrays;
import java.util.List;

import org.springframework.stereotype.Component;

import com.cpe.groupe3.cardmicroservicepublic.dto.CardInstanceDTO;
import com.cpe.groupe3.cardmicroservicepublic.dto.CardInstanceTransactionDTO;
import com.cpe.groupe3.cardmicroservicepublic.rest.ICardInstanceRest;
import com.cpe.groupe3.commonmicroservice.consumer.RestConsumer;

@Component("cardInstanceRestConsumer")
public class CardInstanceRestConsumer extends RestConsumer implements ICardInstanceRest {
    public CardInstanceRestConsumer() {
        super("8083");
    }

    @Override
    public List<CardInstanceDTO> getAllCardInstances() {
        return Arrays.asList(getForEntity(ALL, CardInstanceDTO[].class));
    }

    @Override
    public CardInstanceDTO getCardInstance(Integer id) {
        return getForEntity(GET, CardInstanceDTO.class, id);
    }

    @Override
    public CardInstanceDTO addCardInstance(CardInstanceDTO cardInstance) {
        return postForEntity(ADD, cardInstance, CardInstanceDTO.class);
    }

    @Override
    public void addCardInstancesToNewUser(Integer userId) {
        postForEntity(ADD_NEW_USER, null, void.class, userId);
    }

    @Override
    public CardInstanceDTO updateCardInstance(CardInstanceDTO cardInstance, Integer id) {
        return patchForEntity(UPDATE, cardInstance, CardInstanceDTO.class, id);
    }
    
    @Override
    public void updateCardInstanceForTransaction(CardInstanceTransactionDTO cIT, Integer id) {
        patchForEntity(UPDATE_FOR_TRANSACTION, cIT, CardInstanceTransactionDTO.class, id);
    }

    @Override
    public void deleteCardInstance(Integer id) {
        deleteCardInstance(id);
    }

	@Override
	public List<CardInstanceDTO> getCardInstancesToSell() {
        return Arrays.asList(getForEntity(CARD_INSTANCES_TO_SELL, CardInstanceDTO[].class));
	}

	@Override
	public List<CardInstanceDTO> getCardInstancesForUser(Integer userId) {
        return Arrays.asList(getForEntity(CARD_INSTANCES_FOR_USER, CardInstanceDTO[].class, userId));
	}

}
