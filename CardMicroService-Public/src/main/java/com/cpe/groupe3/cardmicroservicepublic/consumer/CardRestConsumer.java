package com.cpe.groupe3.cardmicroservicepublic.consumer;

import java.util.Arrays;
import java.util.List;

import org.springframework.stereotype.Component;

import com.cpe.groupe3.cardmicroservicepublic.dto.CardDTO;
import com.cpe.groupe3.cardmicroservicepublic.rest.ICardRest;
import com.cpe.groupe3.commonmicroservice.consumer.RestConsumer;

@Component("cardRestConsumer")
public class CardRestConsumer extends RestConsumer implements ICardRest {
	public CardRestConsumer() {
        super("8083");
    }

    @Override
    public List<CardDTO> getAllCards() {
        return Arrays.asList(getForEntity(ALL, CardDTO[].class));
    }

    @Override
    public CardDTO getCard(Integer id){
        return getForEntity(GET, CardDTO.class, id);
    }
}
