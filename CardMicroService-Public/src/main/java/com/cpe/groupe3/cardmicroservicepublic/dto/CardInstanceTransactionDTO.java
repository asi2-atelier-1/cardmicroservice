package com.cpe.groupe3.cardmicroservicepublic.dto;

import java.io.Serializable;

public class CardInstanceTransactionDTO implements Serializable{
	private static final long serialVersionUID = -2711930141719623471L;
	private CardInstanceDTO cID;
	private Integer orderId;
	
	public CardInstanceTransactionDTO() {	
	}

	public CardInstanceTransactionDTO(CardInstanceDTO cID, Integer orderId) {
		this.cID = cID;
		this.orderId = orderId;
	}

	public CardInstanceDTO getCardInstance() {
		return cID;
	}

	public void setCardInstance(CardInstanceDTO cID) {
		this.cID = cID;
	}

	public Integer getOrderId() {
		return orderId;
	}

	public void setOrderId(Integer orderId) {
		this.orderId = orderId;
	}
}
