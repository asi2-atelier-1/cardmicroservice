package com.cpe.groupe3.cardmicroservicepublic.rest;

import java.util.List;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.cpe.groupe3.cardmicroservicepublic.dto.CardDTO;

public interface ICardRest {
	  public final String ROOT_PATH = "/cards";

	    public final String ALL = ROOT_PATH;
	    public final String GET = ROOT_PATH + "/{id}";

	    @RequestMapping(method = RequestMethod.GET, value = ALL)
	    public List<CardDTO> getAllCards();

	    @RequestMapping(method = RequestMethod.GET, value = GET)
	    public CardDTO getCard(Integer id);
}
